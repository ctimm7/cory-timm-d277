<!DOCTYPE html>
<html lang="en">
<head>
    <title>State Website - [Insert City Name Here]</title>
</head>
<body>
    <header>
        <h1>Welcome to beautiful [Insert City Name Here]</h1>
        <!-- Header: Introductory content about the city -->
    </header>
    <nav>
        <ul>
            <li><a href="home.html">Home</a></li>
            <li><a href="state_capital.html">[Capital City Name Here]</a></li>
            <li><a href="firstcity.html">[City 1 Name Here]</a></li>
        </ul>
        <!-- Navigation: Links to navigate the site -->
    </nav>
    <section>
        <h2>About [Insert City Name Here]</h2>
        <p>Information about the city.</p>
        <!-- Main Content: Information about the city -->
    </section>
    <aside>
        <h3>Random City Facts</h3>
        <p>Random facts about [Insert City Name Here].</p>
        <!-- Aside: Other Related information -->
    </aside>
</body>
</html>
